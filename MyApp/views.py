from django.shortcuts import render
from .models import CustomArticle

def CustomHome(request):
    articles = CustomArticle.objects.all()

    context = {
        'articles': articles
    }

    return render(request, 'customHome.html', context)
