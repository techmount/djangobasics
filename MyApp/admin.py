from django.contrib import admin
from .models import Publication, Article, Reporter, MTOArticle, Place, Restaurant, CustomArticle, Customer, Staff, Location, Hotel, MyUser

admin.site.register(Publication)
admin.site.register(Article)
admin.site.register(Reporter)
admin.site.register(MTOArticle)
admin.site.register(Place)
admin.site.register(Restaurant)
admin.site.register(CustomArticle)
admin.site.register(Customer)
admin.site.register(Staff)
admin.site.register(Location)
admin.site.register(Hotel)
admin.site.register(MyUser)

