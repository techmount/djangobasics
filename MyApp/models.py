from django.db import models
from django.contrib.auth.models import User

class Publication(models.Model):
    title = models.CharField(max_length=30)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)


class Article(models.Model):
    headline = models.CharField(max_length=300)
    publications = models.ManyToManyField(Publication)

    def __str__(self):
        return self.headline

    class Meta:
        ordering = ('headline',)


class Reporter(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()

    def __str__(self):
        return self.first_name


class MTOArticle(models.Model):
    headline = models.CharField(max_length=30)
    pub_date = models.DateField()
    reporter = models.ForeignKey(Reporter, on_delete=models.CASCADE)

    def __str__(self):
        return self.headline

    class Meta:
        ordering = ('headline',)


class Place(models.Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Restaurant(models.Model):
    place = models.OneToOneField(Place, on_delete=models.CASCADE, primary_key=True)

    serves_hot_dogs = models.BooleanField(default=False)
    serves_pizza = models.BooleanField(default=False)

    def __str__(self):
        return self.place.name


class CustomArticle(models.Model):
    title = models.CharField(max_length=50)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def ShortenText(self):
        return self.body[:26]


class ContactInfo(models.Model):
    name = models.CharField(max_length=40)
    email = models.CharField(max_length=40)
    address = models.CharField(max_length=30)

    class Meta:
        abstract = True


class Customer(ContactInfo):
    phone = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Staff(ContactInfo):
    position = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=40)
    address = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Hotel(Location):
    serves_dosa = models.BooleanField(default=False)
    serves_chats = models.BooleanField(default=False)


class MyUser(User):
    class Meta:
        ordering = ('username',)

        proxy = True
 
    def fullName(self):
        return self.first_name + " " + self.last_name