from django.urls import path
from .views import CustomHome

urlpatterns = [
    path('customhome/', CustomHome)
]