from django.urls import path
from .views import NewsP, Home, Contact, NewsDate, Register, addUser, modelform, addModelForm

urlpatterns = [
    path('', Home, name='home'),
    path('news/', NewsP, name='news'),
    path('contact/', Contact, name='contact'),
    path('newsdate/<int:year>', NewsDate, name='newsdate'),
    path('signup/', Register, name='register'),
    path('addUser/', addUser, name='addUser'),
    path('modelform/', modelform, name='form'),
    path('addmodelform/', addModelForm, name='modelform')
]