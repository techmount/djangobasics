from django.contrib import admin
from .models import News, RegistrationData

admin.site.register(News)
admin.site.register(RegistrationData)

