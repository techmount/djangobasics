from django.db import models
from django.utils import timezone

class News(models.Model):
    author = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    description = models.TextField()
    pub_date = models.DateField(default=timezone.now())

    def __str__(self):
        return self.author


class RegistrationData(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=20)
    email = models.CharField(max_length=30)
    phone = models.CharField(max_length=10)

    def __str__(self):
        return self.username


