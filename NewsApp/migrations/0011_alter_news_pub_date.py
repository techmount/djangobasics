# Generated by Django 3.2 on 2021-04-23 08:04

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('NewsApp', '0010_alter_news_pub_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='pub_date',
            field=models.DateField(default=datetime.datetime(2021, 4, 23, 8, 4, 4, 457209, tzinfo=utc)),
        ),
    ]
